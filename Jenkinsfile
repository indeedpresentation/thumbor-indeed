import groovyjarjarcommonscli.MissingArgumentException

DOCKER_REPO = "http://106944314577.dkr.ecr.us-west-2.amazonaws.com/indeed-thumbor"
DOCKER_IMAGE_NAME = "indeed-thumbor"

GIT_BRANCH_DEVELOP = "develop"
GIT_BRANCH_MASTER = "master"
GIT_BRANCH_RELEASE = "release"

VERSION_FILE = "version.txt"

ECR_LOGIN_FILE_OUTPUT = "ecr.login"

TASK_DEFINITION_JSON = "task-definition.json"
TASK_DEFINITION_JSON_OUTPUT = "task-definition-"

ECS_TASK_DEFINITION_FAMILY_PRODUCTION = "indeed-thumbor-production"
ECS_SERVICE_NAME_PRODUCTION = "indeed-thumbor-production"
ECS_CLUSTE_NAME_PRODUCTION = "indeed-thumbor-production"
ALLOW_UNSAFE_URL_PRODUCTION = "True"

ECS_TASK_DEFINITION_FAMILY_STAGING = "indeed-thumbor-staging"
ECS_SERVICE_NAME_STAGING = "indeed-thumbor-staging"
ECS_CLUSTE_NAME_STAGING = "indeed-thumbor-staging"
ALLOW_UNSAFE_URL_STAGING = "True"

ECS_TASK_DEFINITION_FAMILY_TESTING = "indeed-thumbor-testing"
ECS_SERVICE_NAME_TESTING = "indeed-thumbor-testing"
ECS_CLUSTE_NAME_PRODUCTION_TESTING = "indeed-thumbor-testing"
ALLOW_UNSAFE_URL_TESTING = "True"

CURRENT_COMMIT_SHA_OUTPUT = "commit.sha"

node('master') {
    try {
        stage 'Checkout'
        checkout()
        def settings = getSettingsForBranch(getCurrentBranch())
        if (settings == null) {
            throw new MissingArgumentException("Current branch not supported for build");
        } else {
            stage 'Build/Push Docker'
            singInToECR()
            docker(DOCKER_REPO, DOCKER_IMAGE_NAME, settings.tag)
            stage 'Update ECS Task Definition'
            updateTaskDefinition(settings.tag, settings.unsafe, settings.awsTaskDefinition)
            stage 'Update ECS Service'
            updateService(settings.awsService, settings.awsTaskDefinition, settings.awsCluster)
        }
    } catch (e) {
        currentBuild.result = "FAIL"
        throw e
    }
}


def getCurrentBranch() {
    if (env.BRANCH_NAME.contains(GIT_BRANCH_DEVELOP)) return GIT_BRANCH_DEVELOP
    if (env.BRANCH_NAME.contains(GIT_BRANCH_MASTER)) return GIT_BRANCH_MASTER
    if (env.BRANCH_NAME.contains(GIT_BRANCH_RELEASE)) return GIT_BRANCH_RELEASE
}

def checkout() {
    checkout scm
}

def getSettingsForBranch(branch) {
    switch (branch) {
        case GIT_BRANCH_DEVELOP:
            return [
                    tag              : "v${getVersion()}-${GIT_BRANCH_DEVELOP}-${getCommitSHA()}",
                    awsService       : ECS_SERVICE_NAME_TESTING,
                    awsTaskDefinition: ECS_TASK_DEFINITION_FAMILY_TESTING,
                    awsCluster       : ECS_CLUSTE_NAME_PRODUCTION_TESTING,
                    unsafe           : ALLOW_UNSAFE_URL_TESTING
            ]
        case GIT_BRANCH_RELEASE:
            return [
                    tag              : "v${getVersion()}-rc",
                    awsService       : ECS_SERVICE_NAME_STAGING,
                    awsTaskDefinition: ECS_TASK_DEFINITION_FAMILY_STAGING,
                    awsCluster       : ECS_CLUSTE_NAME_STAGING,
                    unsafe           : ALLOW_UNSAFE_URL_STAGING
            ]
        case GIT_BRANCH_MASTER:
            return [
                    tag              : "v${getVersion()}",
                    awsService       : ECS_SERVICE_NAME_PRODUCTION,
                    awsTaskDefinition: ECS_TASK_DEFINITION_FAMILY_PRODUCTION,
                    awsCluster       : ECS_CLUSTE_NAME_PRODUCTION,
                    unsafe           : ALLOW_UNSAFE_URL_PRODUCTION
            ]
        default:
            return null
    }
}

def getCommitSHA() {
    sh "git rev-parse --short HEAD >> ${CURRENT_COMMIT_SHA_OUTPUT}"
    def sha = readFile("commit.sha")
    sh "rm ${CURRENT_COMMIT_SHA_OUTPUT}"
    return sha.trim();
}

def getVersion() {
    return readFile(VERSION_FILE)
}


def singInToECR() {
    sh "sudo aws ecr get-login --region us-west-2 >> ${ECR_LOGIN_FILE_OUTPUT}"
    sh "chmod +x ecr.login"
    sh "./${ECR_LOGIN_FILE_OUTPUT}"
    sh "rm ${ECR_LOGIN_FILE_OUTPUT}"
}

def createTaskDefinition(tag, unsafe) {
    sh "sed -e \"s/%TAG%/${tag}/g; s/%ALLOW_UNSAFE_URL%/${unsafe}/g\" ${TASK_DEFINITION_JSON} > ${TASK_DEFINITION_JSON_OUTPUT}${tag}.json"
}

def removeTaskDefinition() {
    sh "rm ${TASK_DEFINITION_JSON_OUTPUT}*.json"
}


def docker(url, name, tag) {
    docker.withRegistry(url) {
        docker.build("${name}:${tag}").push(tag)
    }
}


def updateTaskDefinition(tag, unsafe, family) {
    createTaskDefinition(tag, unsafe)
    sh "cat ${TASK_DEFINITION_JSON_OUTPUT}${tag}.json"
    sh "sudo aws ecs register-task-definition --family ${family} --cli-input-json file://${TASK_DEFINITION_JSON_OUTPUT}${tag}.json"
    removeTaskDefinition()
}

def updateService(service, taskDefinition, cluster) {
    sh "sudo aws ecs update-service --service ${service} --task-definition ${taskDefinition} --region eu-central-1 --cluster ${cluster}"
}
